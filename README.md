# EasyPAP AUR

This repository exists to host an ArchLinux PKGBUILD file for the EasyPAP program.

Links to the official projects:
- [Main page](https://gforgeron.gitlab.io/easypap/)
- [Getting started guide](https://gforgeron.gitlab.io/easypap/doc/Getting_Started.pdf)

## Installation steps for ArchLinux

Dependencies:
- Packaging tools: `base-devel`

Run:
```bash
$ makepkg --install -s
```

To use easypap, call the command `easypap`.
This command is an alias to the `./run` command in the official repository, so it takes exactly the same options.

For example, to run the `seq` variant of the `spin` kernel:
```bash
$ easypap -k spin -v seq
```

To edit your own kernels, we recommend that you create a symbolic link from the `/opt/easypap-se/kernels` directory to your own userspace (so you do not require root access to edit kernels).
